# microHAT SENSOR INTERFACE

This repository contains LTSpice schematic simualtions for a sensor interface that is compliant with the microHAT specifications of the Raspberry Pi. This microHAT was designed with a terrarium owner as the user in mind. Appropriate user stories were applied alongside the collected requirements to develop the specifications for the microHAT. Given this use case, this device is not intended to be weatherproof, but is intended to operate for long periods of time without switching off. As such, the final design operates with USB port that can be attached to a power supply, so it is always on. LTSPice software was used iteratively for simulations in the process of designing this device. 

## USING THIS PROJECT

The use case was broken up into three subsystems: power supply, amplifier and status LEDs. Subsequently, there are respective directories created in this project containing relevant simulation files to understand the design and help in the process of manufacture.

## HOW TO CONTRIBUTE TO THIS PROJECT

1. Report issues in the design by creating an issue request
2. Suggest enhancements to the design by creating an issue request

